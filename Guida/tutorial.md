## Guida sull'utilizzo delle API. 

In informatica, entro un programma, con _application programming interface_ o **API**, si indica un insieme di procedure , in gerene raggruppate per strumenti specifici, atte all'espletamento di un dato compito. Esistono vari design model per le API. Le interfacce intese per la massima velocità di esecuzione spesso consistono in una serie di funzioni, procedure, variabili e strutture dati.

La guida in questione è relativa all'autilizzo delle API in una piccola applicazione react, quindi vado ad illustrare quello che è stato il mio lavoro e la logica di base.

#### Step 1 - preparatory.

Creare una nuova applicazione **React**.

Il paggassio in questione è molto semplice, basterà aprire il terminale e copiare i comandi che sono qui riportati:

1. npx create-react-app *nomeapp* (ad esempio "tutorial-api").
2. cd *nomeapp* .
3. yarn start .

Se il comando yarn non è a disposizione, la nostra vesione di npm è un po' datata, ma su GitHub c'è la guida per le versioni [precedenti](https://github.com/facebook/create-react-app) alla 5.2 .

#### Step 2 - overview.

Utilizzeremo un altro pacchetto di npm, **Axios**, andiamo ad aggiungerlo come prima, dal terminale, copiando questo comando:

1. yarn add axios

Perchè l'utilizzo di un pacchetto npm? Per non impazzire con le *callback*!

L'utilizzo di Axios all'interno del nostro progetto è molto semplice, basterà importarlo, usando un unico comando

1. import axios from 'axios';

all'inteno del nostro file *nomefile*.js.

#### Step 3 - using it.

All'interno di *PersonList*.js possiamo trovare la funzione *componentDidMount*() con la quale andiamo ad effettuare la **.get** da un placeholder generico JSON. Il secondo passo prevede il vero *montaggio* dei dati, con il **setState** all'interno dell'array dichiarato nello stato. L'ultimo passo prevede il **map**, quindi una funzione che ci consente di prendere i nomi delle persone o della compagnia dove lavorano, semplicemente. 

1. **{this.state.persons.map(person => <li>{person.name}</li>)}**

Vediamo dall'esempio che i dati vengono mappati a seconda del nome.

Se sono presenti ancora dei dubbi, molto probabilmente c'è bisogno di dare un'occhiata alla [guida](https://alligator.io/react/axios-react/) che ho seguito anch'io, facendo attenzione anche ai link di riferimento interni. "Axios is promise-based and thus we can take advantage of [async and await](https://alligator.io/js/async-functions/) for more readable asynchronous code." Probabilmente meglio vedere l'uso di queste promises.

#### Step 4 - master it.

Come vedi, all'interno della piccola app React ci sono anche altri file o diverse cartelle. Bene, iniziamo.

Partiamo dalla cartella api, dove troviamo due file, uno di esempio usato nel progetto *::giocagest::* e l'altro è quello che utilizzeremo per capire. Troviamo all'interno di *api.js* lo stesso import di prima, quindi usiamo Axios in questa parte. Il file contiene appunto il link al placeholder e le api, in questo caso la **.get** e la **.delete**. Fin qui niente di nuovo, abbiamo solo il placeholder e le API. Proprio per renderci conto della difficoltà di un progetto, possiamo vedere le API nel file *api-example.js*. Lo sguardo è consigliato per capire come sono scritte le altre API.

**Container**. A questo punto troviamo una cosa nuova, vediamo il file *container.js* e vediamo qualcosa di strano subito alla prima riga. L'utilizzo di **[Unstated](https://github.com/jamiebuilds/unstated)** ci consente di proseguire senza i salvataggi degli stati. 

All'interno di components, troviamo il file *UserField.js* dove abbiamo un componente React, una **select** che fa la chiamata allo store di Unstated, che ha la funzione *_loadUsers()* , che a sua volta richiama le API. L'altra funzione *mapping()* rende compatibili i dati restituiti dalle API con la select, che richiede i campi *value* e *label*.



