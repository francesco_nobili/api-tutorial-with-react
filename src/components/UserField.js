import React from 'react';
import Select from 'react-select';
import {Subscribe,Provider} from 'unstated';
import UserContainer from '../container/container';

export default class UserField extends React.Component {

  render() {
    return (
      <div>
      <Provider>
        <Subscribe to={[UserContainer]}>
          {UsersStore => (
            <Select
              value={this.value}
              valueKey="value"
              labelKey="label"
              closeOnSelect={false}
              //isLoading={UsersStore.state.loading}
              options={UsersStore.state.user}
            />
          )}
        </Subscribe>
      </Provider>
      </div>
    );
  }
}
