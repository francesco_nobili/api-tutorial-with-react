import axios from 'axios';

var url = '';

if (!window.location.origin) {
  window.location.origin =
    window.location.protocol +
    '//' +
    window.location.hostname +
    (window.location.port ? ':' + window.location.port : '');
}
if (window.location.hostname === 'localhost') {
  url = 'http://localhost:8080/index.php/api';
} else {
  url = 'http://giocamondoto.essereweb.it/api';
}

export const API_URL = url;

const http = axios.create({
  baseURL: url,
});

export function getAllServices(archived) {
  return http
    .get(`services${archived ? '?archived=true' : ''}`)
    .then(response => response.data);
}

export function getService(serviceId) {
  return http.get(`services/${serviceId}`).then(response => response.data);
}

export async function saveService(data, id = null) {
  let response;
  if (id) {
    response = await http.put(`services/${id}`, {service: data});
  } else {
    response = await http.post('services', {service: data});
  }

  const json = await response.data;

  return json;
}

export function deleteServices(services) {
  return http
    .post(`services/bulk-delete`, {services: services})
    .then(response => response.data);
}

export function deleteBasicCores(services) {
    return http
        .post(`basic/cores/bulk-delete`, {basicCores: services})
        .then(response => response.data);
}

export async function exportBasicCores(basiccores) {
 alert('non ancora disponibile')
}

export function deleteService(service) {
  return http.delete(`services/${service}`).then(response => response.data);
}

export async function exportServices(services) {
  const response = await http.post('/services/bulk-export', {services});

  const url = window.URL.createObjectURL(
    new Blob([response.data], {
      type: 'text/csv',
    })
  );
  window.open(url);
}

export function cloneService(service) {
  return http
    .post('/services/clone', {service})
    .then(response => response.data);
}

export function getServiceTypes() {
  return http.get('service/types').then(response => response.data);
}

export function getAllTags() {
  return http.get('tags').then(response => response.data);
}

export function getServiceProviders() {
  return http.get('service/providers').then(response => response.data);
}

// Nucleo Base

export function getNucleiBase() {
  return http.get(`basic/cores`).then(response => response.data);
}

export function getNucleoBase(id) {
  return http.get(`basics/${id}/core`).then(response => response.data);
}

export async function saveNucleoBase(data, id = null) {
  let response;
  if (id) {
    response = await http.put(`basics/${id}/core`, {basic_core: data});
  } else {
    response = await http.post('basics/cores', {basic_core: data});
  }

  const json = await response.data;

  return json;
}

export function deleteNucleoBase(nucleo) {
  return http.delete(`services/${nucleo}`).then(response => response.data);
}

export function cloneNucleoBase(nucleo) {
  return http
    .post('/basic/cores/clone', {core: nucleo})
    .then(response => response.data);
}

// Packages

export function getPackages() {
  return http.get(`packages`).then(response => response.data);
}

export function getPackage(id) {
  return http.get(`packages/${id}`).then(response => response.data);
}

export async function savePackage(data, id = null) {
  let response;
  if (id) {
    response = await http.put(`packages/${id}`, {package: data});
  } else {
    response = await http.post('packages', {package: data});
  }

  const json = await response.data;

  return json;
}

export function deletePackage(p) {
  return http.delete(`packages/${p}`).then(response => response.data);
}

export function clonePackage(p) {
  return http
    .post('/packages/clone', {package: p})
    .then(response => response.data);
}
