import axios from 'axios';

const http= axios.create({
  baseURL: `http://jsonplaceholder.typicode.com/`
});

const url=http;

function deleteUser() {
    return http
      .postAPI.delete(`users/${this.state.id}`)
      .then(response => response.data);
}

function getAllUsers() {
    return http
      .get(`users`)
      .then(response => response.data);
}

export default {url,deleteUser,getAllUsers}