import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PersonList from './PersonList';
import UserField from './components/UserField'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <h3>
          I'm trying to get some names from a generic json placeholder with "axios"
        </h3>
        <PersonList className="Person-List"/>
        <UserField/>
      </div>
    );
  }
}

export default App;
