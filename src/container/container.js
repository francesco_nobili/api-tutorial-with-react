import {Container} from 'unstated';
import API from '../api/api';


export default class UserContainer extends Container {
  state = {
    loading: true,
    user: [],
  
  };

  constructor() {
    super();
    this._loadUsers();
  }
  
  async _loadUsers() {
    const user = await API.getAllUsers();
    const finalUsers = this.mapping(user);
    await this.setState({user:finalUsers, loading: false});
    //console.log(user)
  }
  
  getById(id) {
    return this.state.services.filter(s => s.id === id)[0];
  }

  mapping(user){
    return user.map(item => ({
      value: item.id,
      label: item.name
    }))
  }

}
