import React from 'react';
import axios from 'axios';


export default class PersonList extends React.Component {
  state = {
    persons: []
  }

  componentDidMount() {
    axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
        const persons = res.data;
        this.setState({ persons });
      })
  }

  render() {
    return (
    <div className="left">
        <h4>
            Names are here:
        </h4>
        <ul>
            {this.state.persons.map(person => <li>{person.name}</li>)}
        </ul>
        <h4>
            The company for which they work:
        </h4>
        <ul>
            {this.state.persons.map(person => <li>{person.company.name}</li>)}
        </ul>
    </div>
    )
  }
}